import csv
import glob
import os
from sklearn.model_selection import train_test_split


def main():
    columns = ['name', 'category']

    with open('train.csv', 'w+') as train_cvs, open('val.csv', 'w+') as val_cvs:
        train_wt = csv.writer(train_cvs)
        train_wt.writerow(columns)

        val_wt = csv.writer(val_cvs)
        val_wt.writerow(columns)

        for fname in glob.glob('dataset/names/*.txt'):
            category = os.path.splitext(os.path.basename(fname))[0]

            with open(fname) as f:
                lines = f.readlines()

                train, val = train_test_split(lines, test_size=0.1)

                for line in train:
                    train_wt.writerow([line.strip(), category])

                for line in val:
                    val_wt.writerow([line.strip(), category])


if __name__ == '__main__':
    main()
