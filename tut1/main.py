from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
from torch.nn import functional as F
from torch.nn.utils.rnn import pad_sequence
import torch
from glob import glob
import os
import unicodedata
import string

all_letters = string.ascii_letters + " .,;'"
n_letters = len(all_letters)


def unicode_to_ascii(s):
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
        and c in all_letters
    )


# Just for demonstration, turn a letter into a <1 x n_letters> Tensor
def letter2tensor(letter):
    return F.one_hot(torch.tensor(all_letters.find(letter)), num_classes=n_letters)


# Turn a line into a <line_length x 1 x n_letters>,
# or an array of one-hot letter vectors
def line2tensor(line):
    tensor = torch.zeros(len(line), n_letters)
    for li, letter in enumerate(line):
        tensor[li][letter2tensor(letter)] = 1
    return tensor


class MyDataset(Dataset):
    def __init__(self, path):
        self.items = []

        category_count = len(glob(os.path.join(path, '*.txt')))

        categories = {}
        for cat_id, fname in enumerate(glob(os.path.join(path, '*.txt'))):
            category = os.path.splitext(os.path.basename(fname))[0]

            with open(fname) as f:
                for l in f:
                    categories[category] = category

                    self.items.append({
                        'line': line2tensor(unicode_to_ascii(l.strip())),
                        'category': cat_id,
                    })

        self.categories = list(categories.values())

    def __len__(self):
        return len(self.items)

    def __getitem__(self, item):
        return self.items[item]


ds = MyDataset('/home/vladimirov/workspace/tf/torch/tut1/dataset/names')


def pad_collate(batch):
    xx = [x['line'] for x in batch]
    x_lens = [len(x['line']) for x in xx]

    yy = [x['category'] for x in batch]

    xx_pad = pad_sequence(xx, batch_first=True, padding_value=0)
    return xx, yy, x_lens


loader = DataLoader(ds, batch_size=10, shuffle=True, collate_fn=pad_collate)
for i, batch in enumerate(loader):
    print(i, batch)
    break


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size + hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size + hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        combined = torch.cat((input, hidden), 1)
        hidden = self.i2h(combined)
        output = self.i2o(combined)
        output = self.softmax(output)
        return output, hidden

    def init_hidden(self):
        return torch.zeros(1, self.hidden_size, dtype=torch.LongTensor)


n_hidden = 128
all_letters = string.ascii_letters + " .,;'"
n_letters = len(all_letters)

rnn = RNN(n_letters, n_hidden, len(ds.categories))
