from torchtext.data import TabularDataset, Field, BucketIterator
import unicodedata
import string
import torch
from torch import nn
from torch.nn import functional as F
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter
from collections import defaultdict


def unicode_to_ascii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in string.ascii_letters)


tokenize = lambda x: list(unicode_to_ascii(x))
TEXT = Field(sequential=True, tokenize=tokenize, lower=True, eos_token=True)
LABEL = Field(sequential=False, use_vocab=True, lower=True, is_target=True)


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()

        self.hidden_size = hidden_size
        self.num_layers = 4

        self.lstm = nn.LSTM(input_size, hidden_size, self.num_layers, bidirectional=True, dropout=0.5)
        self.i2o = nn.Linear(2*hidden_size, 32)
        self.i2oo = nn.Linear(32, output_size)
        self.softmax = nn.LogSoftmax(dim=2)

    def forward(self, input):
        output, _ = self.lstm(input)
        output = self.i2o(output)
        output = self.i2oo(output)
        output = self.softmax(output)
        return output


# TODO: ngrams, validation, dropout
def main():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    fields = [
        ('name', TEXT),
        ('category', LABEL)
    ]

    val_ds = TabularDataset(path='val.csv', skip_header=True, format='csv', fields=fields)

    train_ds = TabularDataset(path='names.csv', skip_header=True, format='csv', fields=fields)
    TEXT.build_vocab(train_ds, val_ds)
    LABEL.build_vocab(train_ds, val_ds)

    print(LABEL.vocab.itos)
    batch_size = 1500
    train_it = BucketIterator(train_ds, batch_size, shuffle=True, train=True, device=device)
    val_it = BucketIterator(val_ds, batch_size, train=True, device=device)

    n_chars = len(TEXT.vocab.itos)
    n_classes = len(LABEL.vocab.itos)

    n_hidden = 128
    n_epochs = 10000
    learning_rate = 0.001  # If you set this too high, it might explode. If too low, it might not learn
    print_every = 5000
    plot_every = 1000

    rnn = RNN(n_chars, n_hidden, n_classes).to(device)
    optimizer = torch.optim.Adam(rnn.parameters(), lr=learning_rate)
    criterion = nn.NLLLoss()

    def category_label_from_output(output: torch.Tensor):
        top_n, top_i = output.topk(1)
        return LABEL.vocab.itos[top_i[0].item()]

    def validate():
        rnn.eval()

        total = 0
        accuracy_by_category = defaultdict(list)
        for batch in val_it:
            inp = F.one_hot(batch.name, n_chars).float()
            # (SEQ_LEN, BATCH_N, N_CLASSES)
            output = rnn(inp)

            for i in range(len(batch)):
                predicted_cat = category_label_from_output(output[-1][i])
                cat = LABEL.vocab.itos[batch.category[i]]
                if predicted_cat == cat:
                    accuracy_by_category[cat].append(1)
                    total += 1/len(val_ds)
                else:
                    accuracy_by_category[cat].append(0)

        mean = []
        for k, v in accuracy_by_category.items():
            print(k, (sum(v) / len(v)) * 100)
            mean.append((sum(v) / len(v)) * 100)

        print('Total:', total)
        print('MEAN:', sum(mean) / len(mean))

    validate()

    # return

    def eval():
        vec = TEXT.numericalize(['korenberg'])
        inp = F.one_hot(vec, n_chars).float().to(device)

        output = rnn(inp)
        top_n, top_i = output.topk(3)
        print(LABEL.vocab.itos[top_i[0][-1][0].item()], top_n[0][-1][0].item())
        print(LABEL.vocab.itos[top_i[0][-1][1].item()], top_n[0][-1][1].item())
        print(LABEL.vocab.itos[top_i[0][-1][2].item()], top_n[0][-1][2].item())
        # return all_categories[category_i], category_i

    eval()
    # return
    loss = None
    losses = []

    wt = SummaryWriter('logs')
    try:
        for epoch in range(1, n_epochs):
            rnn.train()  # turn on training mode

            if losses:
                wt.add_scalar('training_loss', sum(losses) / len(losses), epoch * len(train_it))
                losses = []

            for batch in tqdm(train_it):
                optimizer.zero_grad()
                inp = F.one_hot(batch.name, n_chars).float().to(device)
                output = rnn(inp)

                # print(output.size())
                # print(batch.category.size())
                loss = criterion(output[-1], batch.category)
                loss.backward()
                optimizer.step()
                losses.append(loss.data)
            validate()
    finally:
        wt.close()
        # torch.save(rnn.state_dict(), 'rnn.pt')


if __name__ == '__main__':
    main()
